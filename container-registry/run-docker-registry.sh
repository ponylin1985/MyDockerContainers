#!/bin/bash

# 啟動 docker registry container
docker run \
    -d \
    -p 3300:5000 \
    -v /mnt/data-disk/docker/registry/docker_images:/var/lib/registry \
    --name registry \
    --restart=always \
    registry:2

# 啟動 docker registry web console container
docker run \
    -d \ 
    -p 8088:8080 \
    --name registry-web \
    --link registry-srv \
    -e REGISTRY_URL=http://registry-srv:5000/v2 \
    -e REGISTRY_NAME=localhost:5000 \
    hyper/docker-registry-web 