# HasiCorp Vault

----------------

## Root Token

- root token 是一個在 vault 服務中最大權限的 token，需要在啟動 vault 服務時 (也就是啟動 vault container 時) 指定給 vault。
- root token 可以使用一個 UID，或者強度更高的亂數字串。
- root token: `d9475700-16e9-4325-9ab0-f7da3f60037a`

----------------

## docker-compose 啟動方式

- 在 docker-host 啟動時，先在 docker-host 上設定一個環境變數 `VAULT_DEV_ROOT_TOKEN_ID=d9475700-16e9-4325-9ab0-f7da3f60037a`
  - 用來當作 `docker-compose up -d` 啟動 container 時當作參數傳遞給 container 內部當作環境變數。

- 根據 ChatGPT 老師的建議，也可以直接使用以下語法，就可以把 root token 當作參數傳遞給 vault container 內部當作環境參數了。
  - `VAULT_DEV_ROOT_TOKEN_ID=d9475700-16e9-4325-9ab0-f7da3f60037a docker-compose up -d`
