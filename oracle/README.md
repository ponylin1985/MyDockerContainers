#  Oracle Express Edition (XE) 18.4.0.0.0 (18c) docker container 安裝參考文件

##  Step 1: Download Oracle Express Edition version 18c (xe) Linux rpm

-  Go to [Oracle's Web](https://www.oracle.com/database/technologies/xe-downloads.html) download the
Oracle Database 18c Express Edition for Linux x64.

##  Step 2: Git clone the Oracle Database's dockerfiles.

   Use the follow command to git clone the dockerfiles.

   `git clone https://github.com/oracle/docker-images.git`

##  Step 3: Copy Binary to Dockerfiles dir

   -  Copy the **Oracle Express Edition version 18c (xe) Linux rpm** file to `/dockerfiles` directory for _docker build_.s

   -  Use the following command to copy files.

      `cd ./OracleDatabase/SingleInstance/dockerfiles`

      `cp ~/Downloads/oracle-database-xe-18c-1.0-1.x86_64.rpm ./18.4.0`

##  Step 4: Build the Oracle Database docker images.

   -  Build docker image for oracle database. Be patient, the docker build takes **15 minutes at least...**

      `./buildDockerImage.sh -x -v 18.4.0`

   -  After finished docker build. Verify the docker image is existed. The size of docker image is around **3 ~ 5 GB...**

      `docker images`

   -  Use `docker run` or `docker-compose up -d` to start the Oralce 18c container.
      -  If you try to run this container on Docker for macOS, you need to **configure 2 GB memory to the docker engine at least**. Otherwise you may fail to start the container.
      -  Be patient, the docker build takes **15 to 20 minutes at least...**

##  Step 5: Connect to DB

   -  UID: `sys as SYSDBA`
   -  Password: `Admin123`
   -  SID: `XE`

##  Step 6: Create new user for login.

   -  First use `SYS AS SYSDBA` login to the Oracle 18c database. (SID is `XE`)
   -  List the PDBS for Oracle.

      `SHOW pdbs;`

      Show output: `XEPDB1`  read write

      `SHOW con_names;`

   -  Alter the Oracle connection and create new user account for Oracle 18c.

      ```sql
      alter session set container=XEPDB1;
      create user {user_name} identified by "{user_password}" default tablespace USERS;
      grant connect,resource,unlimited tablespace to {user_name};
      ```

      Example:

      -  user_name: pony
      -  user_password: Admin123

      ```sql
      alter session set container=XEPDB1;
      create user pony identified by "Admin123" default tablespace USERS;
      grant connect,resource,unlimited tablespace to pony;
      ```

##  Step 7: Login with user account.

   -  UID: `pony`
   -  Password: `Admin123`
   -  SERVICE_NAME: `XEPDB1` **<font color=red>(USE THE SERVICE_NAME to login to Oracle 18c `XEPDB1` database here.)</font>**

##  Step 8: Set Password Life Time (Set to Never expire.)

   ```sql
   select * from dba_profiles;
   ALTER PROFILE DEFAULT LIMIT PASSWORD_LIFE_TIME UNLIMITED;
   ```

   - Reference:
     - https://stackoverflow.com/questions/40581131/ora-28001-the-password-has-expired

##  Open Oracle SQLDeveloper in MacOS Big Sur 11.0.1

- `sudo zsh /Applications/SQLDeveloper.app/Contents/Resources/sqldeveloper/sqldeveloper.sh`
- Reference: [SQL Developer on macOS 11 Big Sur?](https://community.oracle.com/tech/developers/discussion/4477413/sql-developer-on-macos-11-big-sur)


##  References

   -  [How to run Oracle DB on a Mac with Docker](https://www.petefreitag.com/item/886.cfm)
   -  [Oracle 18c PDB login](https://blog.darkthread.net/blog/oracle-cbd-and-pdb/)


