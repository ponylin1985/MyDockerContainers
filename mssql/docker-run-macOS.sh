#!/bin/bash

# This is the docker commands for MSSQL linux docker container runs in macOS.

# 執行這一行的時候，要登出 DockerHub 的帳號才可以正常執行
# docker create -v /var/opt/mssql --name mssql mcr.microsoft.com/mssql/server:2019-latest /bin/true
docker create -v /var/opt/mssql --name mssql-data-2019 \
  mcr.microsoft.com/mssql/server:2019-latest \
  /bin/true

# docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Admin123' -p 1433:1433 --volumes-from mssql -d --name sql-server mcr.microsoft.com/mssql/server:2019-latest
docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin123' \
  -p 1433:1433 \
  --volumes-from mssql-data-2019 \
  -d \
  --name mssql-2019 \
  --restart=always \
  mcr.microsoft.com/mssql/server:2019-latest

docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin123' \
  -p 1433:1433 \
  --volumes-from mssql-data-2019 \
  -d \
  --name mssql-2019 \
  mcr.microsoft.com/mssql/server:2019-latest