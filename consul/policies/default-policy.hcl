# policy.hcl
# 此 Policy 會讓非 master token 登入的 user 只能從 web console 瀏覽 Services 和 Nodes，當作是 Consul ACL 預設的權限。

# 允許訪問 Services
service_prefix "" {
  policy = "read"
}

# 允許訪問 Nodes
node_prefix "" {
  policy = "read"
}

# 拒絕所有其他訪問
key_prefix "" {
  policy = "deny"
}

