# 這個 Policy 可以在使用 master ACL token 登入 web console 管理介面後，使用 ACL 頁面建立一個以下的 Policy，然後再套用到 Role 上。
# 然後就可以使用此 Policy 和 Role 建立一個給一般 user 使用的 ACL token，進行權限控管。
# 允許使用者查看 Services
node_prefix "" {
  policy = "read"
}

# 允許使用者查看 Nodes
service_prefix "" {
  policy = "read"
}