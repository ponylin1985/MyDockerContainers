#!/bin/bash

# https://learn.hashicorp.com/tutorials/consul/docker-container-agents
# https://hub.docker.com/r/bitnami/consul

# docker run -d -p 8500:8500 -p 8600:8600/udp --name=consul-test consul:1.8.4 agent -server -ui -node=server-1 -bootstrap-expect=1 -client=0.0.0.0
# docker container 啟動之後 web console 可以使用 http://localhost:8500 進行瀏覽。

docker run \
  -d \
  -p 8500:8500 \
  -p 8600:8600/udp \
  --name=badger \
  consul:1.8.4 agent -server -ui -node=server-1 -bootstrap-expect=1 -client=0.0.0.0