#  MongoDB 在 docker container 中建立 user 與 password 說明文件 


##  Step 1: 進入到 mongodb 的 docker container 中

   `docker exec -it mongodb sh`

##  Step 2: 執行 mongodb 的 CLI
   
   `mongo`

##  Step 3: 使用 admin 角色

   `use admin`

##  Step 4: 建立 mongodb 的使用者與密碼

   ```json
   db.createUser(
      {
         user: "root", 
         pwd: "Admin123", 
         roles:["root"]
      }
   );
   ```

## Reference: 

   -  [https://medium.com/rahasak/enable-mongodb-authentication-with-docker-1b9f7d405a94](https://medium.com/rahasak/enable-mongodb-authentication-with-docker-1b9f7d405a94)