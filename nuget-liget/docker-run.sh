#!/bin/bash

# https://blog.txstudio.tw/2019/03/use-docker-run-liget-nuget-server.html
# https://github.com/ai-traders/liget#usage
docker run -d \
   -p 9011:9011 \
   --name nuget-server \
   -v /mnt/data-disk/docker/nuget-liget/nuget_data:/data/simple2 \
   -v /mnt/data-disk/docker/nuget-liget/nuget_cache:/cache/simple2 \
   -v /mnt/data-disk/docker/nuget-liget/nuget_sqlite:/data/ef.sqlite \
   -e LIGET_API_KEY_HASH=3b612c75a7b5048a435fb6ec81e52ff92d6d795a8b5a9c17070f6a63c97a53b2 \
   tomzo/liget

# API Key 目前設定為: Admin123
# API Key Hash 為: 3b612c75a7b5048a435fb6ec81e52ff92d6d795a8b5a9c17070f6a63c97a53b2
# 使用範例:
# dotnet nuget push SomeTest.3.1.2.nupkg --source http://localhost:9011/api/v3/index.json --api-key Admin123 --interactive
# 在 linux 的 terminal 可以執行以下指令產生 API Key Hash
# -n 'Admin123' | sha256sum

   