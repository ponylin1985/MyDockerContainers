#!/bin/bash

# This is the docker commands for MSSQL linux docker container runs in macOS.
# Run SQL Server 2017 linux docker container with only 1 GB memory solution.
# https://github.com/justin2004/mssql_server_tiny

# docker create -v /var/opt/mssql --name mssql-data mcr.microsoft.com/mssql/server:2017-latest /bin/true
docker create -v /var/opt/mssql --name mssql-data \
  mcr.microsoft.com/mssql/server:2017-latest \
  /bin/true

# docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Admin123' -p 1433:1433 --volumes-from mssql-data -d --name sql-server-tiny --restart=always ponylin1985/mssql-tiny:2017-latest-ubuntu
docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin123' \
  -p 1433:1433 \
  --volumes-from mssql-data \
  -d \
  --name mssql-tiny-2017 \
  --restart=always \
  ponylin1985/mssql-tiny:2017-latest-ubuntu

# docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Admin123' -p 1433:1433 --volumes-from mssql-data -d --name sql-server-tiny ponylin1985/mssql-tiny:2017-latest-ubuntu
docker run \
  -e 'ACCEPT_EULA=Y' \
  -e 'SA_PASSWORD=Admin123' \
  -p 1433:1433 \
  --volumes-from mssql-data \
  -d \
  --name mssql-tiny-2017 \
  ponylin1985/mssql-tiny:2017-latest-ubuntu